/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 116397543
 */
public class CalculatorTest {
    
    public CalculatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calculateStandardVAT method, of class Calculator.
     */
    @Test
    public void testCalculateStandardVAT() {
        System.out.println("calculateStandardVAT");
        double price = 100.0;
        double expResult = 23;
        double result = Calculator.calculateStandardVAT(price);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to //fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of calculateMealVAT method, of class Calculator.
     */
    @Test
    public void testCalculateMealVAT() {
        System.out.println("calculateMealVAT");
        double price = 10.0;
        double expResult = 1.35;
        double result = Calculator.calculateMealVAT(price);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to //fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addThree method, of class Calculator.
     */
    @Test
    public void testAddThree() {
        System.out.println("addThree");
        int a = 1;
        int b = 2;
        int c = 3;
        int expResult = 6;
        int result = Calculator.addThree(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to //fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of multiplyThree method, of class Calculator.
     */
    @Test
    public void testMultiplyThree() {
        System.out.println("multiplyThree");
        int a = 1;
        int b = 2;
        int c = 3;
        int expResult = 6;
        int result = Calculator.multiplyThree(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to //fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of returnMax method, of class Calculator.
     */
    @Test
    public void testReturnMax() {
        System.out.println("returnMax");
        int a = 5;
        int b = 10;
        int c = 15;
        int expResult = 15;
        int result = Calculator.returnMax(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to //fail.
        //fail("The test case is a prototype.");
    }
    
}
